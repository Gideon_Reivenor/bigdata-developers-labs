import sbt._

import scala.languageFeature.postfixOps

object Dependecies {
  object versions {
    val sparkVersion = "2.4.5"
    val scalatestVersion = "3.1.1"
    val testingBaseVersion = "2.3.1_0.12.0"
    val logbackVersion = "1.2.3"
    val mockito = "1.11.4"
  }

  lazy val sparkCore = "org.apache.spark" %% "spark-core" % versions.sparkVersion
  lazy val sparkHive = "org.apache.spark" %% "spark-hive" % versions.sparkVersion
  
  lazy val scalatest = "org.scalatest" %% "scalatest" % versions.scalatestVersion
  lazy val mockito = "org.mockito" %% "mockito-scala" % versions.mockito
  lazy val sparkTestingBase = "com.holdenkarau" %% "spark-testing-base" % versions.testingBaseVersion

  object java {
    lazy val logback = "ch.qos.logback" % "logback-classic" % versions.logbackVersion
  }

  def globalProjectDeps: Seq[ModuleID] = Seq(
    sparkCore,
    sparkHive,
    java.logback
  )

  def globalTesttDeps: Seq[ModuleID] = Seq(
    scalatest,
    mockito,
    sparkTestingBase
  ) map (_ % "test")
}
