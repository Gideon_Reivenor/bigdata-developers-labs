package ru.philit.bigdata.vsu.other.airlines

object Boot extends App {

  val airlines = AirlineService(
    InputSet(
      Route.load("./datasource/avia/routes.dat.txt"),
      Airline.load("./datasource/avia/airlines.dat.txt"),
      Airport.load("./datasource/avia/airports.dat.txt"),
      loadCounties("./datasource/avia/countries.dat.txt")
    )
  )

  val joined = airlines.joinSources("AFL")

  //println(joined.mkString("\n"))

  println(airlines.aggregate(joined).mkString("\n"))
}
