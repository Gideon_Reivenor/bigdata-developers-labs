package ru.philit.bigdata.vsu.scalaLang.funcStrucut


object TestApp extends App {

  import FuncSetImpl._

  val numerals = Seq(1, 2, 3, 4, 5)


  val sFi = singletonSet[Int](1)
  val sSec = singletonSet[Int](2)
  val sTh = singletonSet[Int](3)
  val sFo = singletonSet[Int](4)

  val first = /*union(*/union(sFo, sSec)//, sTh)

  println(asString(first)(numerals))

  println(!forAll(first, (x: Int) => x % 2 == 0)(numerals))

  println(exists(first, (x: Int) => x % 2 != 0)(numerals))

  println(
    String.format("%d %d smth", java.lang.Integer.valueOf("1"), java.lang.Integer.valueOf("3")))




}
