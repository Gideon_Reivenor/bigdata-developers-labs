package ru.philit.bigdata.vsu.scalaLang.oop

import ru.philit.bigdata.vsu.scalaLang.oop.Cake.ComponentRegistry

object Boot {
  def main(args: Array[String]): Unit = {
    ComponentRegistry.dataSetLoader.loadCountries("./datasource/avia/countries.dat.txt")
      .foreach(println)
  }
}
