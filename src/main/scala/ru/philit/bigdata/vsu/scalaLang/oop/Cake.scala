package ru.philit.bigdata.vsu.scalaLang.oop

import java.io.{File, PrintWriter}
import java.time.Instant
import java.util.concurrent.atomic.AtomicInteger

import ru.philit.bigdata.vsu.scalaLang.oop.Cake.ComponentRegistry

import scala.io.Source
import scala.util.{Failure, Success, Try}

package object Cake {

  trait LoggerProviderComponent {
    val logger: LoggerProvider

    trait LoggerProvider {
      def debug(msg: String): Unit

      def info(msg: String): Unit

      def error(msg: String, throwable: Throwable = None.orNull): Unit

      def flush(): Unit
    }

  }

  trait DataSetLoaderComponent {
    val dataSetLoader: DataSetLoader

    trait DataSetLoader {
      def loadCountries(path: String): Map[String, String]
    }

  }

  trait LoggerProviderComponentImpl extends LoggerProviderComponent {

    class LoggerProviderImpl extends LoggerProvider {
      val printer = new PrintWriter(new File("./application.log"))

      private def print(msg: String, flag: String = "MSG") =
        printer.write(s"${Instant.now().toString} $flag: $msg\n")

      override def debug(msg: String): Unit =
        print(msg, "DEBUG")

      override def info(msg: String): Unit =
        print(msg, "INFO")

      override def error(msg: String, throwable: Throwable): Unit =
        print(s"$msg : ${throwable.getLocalizedMessage}", "ERROR")

      override def flush(): Unit = printer.close()
    }

  }

  trait DataSetLoaderComponentImpl extends DataSetLoaderComponent {
    this: DataSetLoaderComponentImpl with LoggerProviderComponent =>

    class DataSetLoaderImpl extends DataSetLoader {

      override def loadCountries(path: String): Map[String, String] = {
        logger.info("Load started")
        val broken = new AtomicInteger(0)
        Try {
          Source.fromFile(path)
            .getLines().flatMap {
            _.replace("\"", "").split(",") match {
              case Array(name, iso, _) =>
                Some((iso, name))
              case _ =>
                broken.incrementAndGet()
                None
            }
          }.toMap
        } match {
          case Success(value) =>
            logger.info(s"Success. Broken Strings ${broken.get()}")
            logger.flush()
            value
          case Failure(ex) =>
            logger.error(s"Failed to load in cause of:", ex)
            logger.flush()
            Map.empty[String, String]
        }
      }
    }

  }

  trait ConsoleLoggerComponentImpl extends LoggerProviderComponent {
    val logger: LoggerProvider

    class ConsoleLoggerImpl extends LoggerProvider {
      private def print(msg: String, flag: String = "MSG"): Unit =
        println(s"${Instant.now().toString} $flag: $msg\n")

      override def debug(msg: String): Unit =
        print(msg, "DEBUG")

      override def info(msg: String): Unit =
        print(msg, "INFO")

      override def error(msg: String, throwable: Throwable): Unit =
        print(s"$msg : ${throwable.getLocalizedMessage}", "ERROR")

      override def flush(): Unit = ()
    }
  }

  object ComponentRegistry extends DataSetLoaderComponentImpl with ConsoleLoggerComponentImpl {
    override val logger: LoggerProvider = new ConsoleLoggerImpl
    override val dataSetLoader: ComponentRegistry.DataSetLoader = new DataSetLoaderImpl
  }

}
