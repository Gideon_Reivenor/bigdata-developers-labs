package ru.philit.bigdata.vsu.scalaLang.oop

object OopExampe extends App {
  val lightTruck: Automobile = Truck(5).repair.refuel(50)
  println(lightTruck.status)

  println(lightTruck.drive(Position(0), Position(100), 20))

  println(lightTruck.status)
}
