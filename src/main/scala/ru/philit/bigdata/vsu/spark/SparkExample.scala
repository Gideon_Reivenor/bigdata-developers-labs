package ru.philit.bigdata.vsu.spark

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FileSystem, Path}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}

object SparkExample {
  def main(args: Array[String]): Unit = {
    val hdpConf = new Configuration()
    val hdfs = FileSystem.get(hdpConf)

    val sparkConf = new SparkConf()
      .setAppName("spark-example")
      .setMaster("local[*]")
    val sc = new SparkContext(sparkConf)

    val parallRdd: RDD[Int] = sc.parallelize(Seq(1, 2, 3, 4, 5))

    val loadedRdd: RDD[String] = sc.textFile("./datasource/avia/countries.dat.txt")

    case class Country(name: String, iso: String) {
      override def toString: String = s"$name : $iso"
    }

    // val seqFileRdd = sc.sequenceFile[Int, String]("./datasource/avia/countries.dat.txt")
    val counties: RDD[Country] = loadedRdd.flatMap {
      _.replace("\"", "").split(",", -1) match {
        case Array(name, iso, _) => Some(Country(name, iso))
        case _ => None
      }
    }.filter(country => country.name.toLowerCase().startsWith("r"))
    
    val outputPath = "./spark_output"

    if(hdfs.exists(new Path(outputPath)))
      hdfs.delete(new Path(outputPath), true)

    counties.saveAsTextFile(outputPath)


    sc.stop()
    hdfs.close()
  }
}
