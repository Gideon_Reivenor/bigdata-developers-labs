package ru.philit.bigdata.vsu.spark.exercise

import org.apache.log4j.{Level, Logger}
import org.apache.spark.rdd.RDD
import org.apache.spark.{SparkConf, SparkContext}
import ru.philit.bigdata.vsu.other.airlines.{Airline, Route}

import scala.util.Try

object Example extends App {
  Logger.getLogger("org").setLevel(Level.ERROR)
  Logger.getLogger("netty").setLevel(Level.ERROR)

  val sparkConf = new SparkConf()
    .setAppName("spark-example")
    .setMaster("local[*]")
  val sc = new SparkContext(sparkConf)

  case class Route(
                    airlineCode: String,
                    airlineId: Int,
                    sourceAirportCode: String,
                    sourceAirportId: Int,
                    targetAirportCode: String,
                    targetAirportId: Int
                  )

  case class Airline(
                      id: Int,
                      name: String,
                      iata: String,
                      icao: String,
                      country: String
                    )

  val airlines = sc.textFile("./datasource/avia/airlines.dat.txt")
    .flatMap {
      _.replace("\"", "").split(",", -1) match {
        case Array(id, name, _, iata, icao, _, country, active) if active.equals("Y") =>
          Try(Airline(id.toInt, name, iata, icao, country)).toOption
        case _ => None
      }
    }

  val routes = sc.textFile("./datasource/avia/routes.dat.txt")
    .flatMap {
      _.replace("\"", "").split(",", -1) match {
        case Array(airCode, airId, sourceAirport, sourceAirportId, targetAirport, targetAirportId, _*) =>
          Try(Route(airCode, airId.toInt, sourceAirport, sourceAirportId.toInt, targetAirport, targetAirportId.toInt))
            .toOption
        case _ => None
      }
    }

  routes.map {
    case route@Route(airlineCode, _, _, _, _, _) => (airlineCode, route)
  }.fullOuterJoin(airlines.map {
    case airline@Airline(_, _, iata, _, _) => (iata, airline)
  })

  airlines.map {
    case airline@Airline(_, _, _, _, country) => (country, airline)
  }.aggregateByKey(0)(
    (acc, airline) => if(airline.name.toLowerCase.startsWith("b")) acc + 1 else acc,
    (acc1, acc2) => acc1 + acc2
  ).filter(_._2 > 1).foreach(println)


}
