package ru.philit.bigdata.vsu.spark.exercise

object Lab12SQLCollections {
  /*
  * Lab12 - пример использования explode и struct
  * Необходимо вывести историю выполненных заказов для клиента c именем John
  * Итоговое множество содержит поля: customer.name, effective_date, expiration_date, status
  *
  * 1. Создать неявный объект SparkSession, используя метод builder()
  * 2. Установить master в local[*]
  * 3. Установить имя приложения spark-sql-labs
  * 4. Создать неявный объект класса Parameters, используя метод Parameters.instance
  * 5. Вызвать метод initTables класса Parameters
  * 6. Загрузить таблицу customer в DataFrame
  * 7. Выбрать записи с именем John
  * 8. Загрузить таблицу order в DataFrame
  * 9. Выполнить внутреннее соединение DataFrame из п.7 и п.8
  * по полю customer.id и order.customer_id
  * 10. Выбрать заказы со статусом delivered
  * 11. Загрузить таблицу order-info в DataFrame
  * 12. Выбрать полу id далее id, создать массив со структурой effective_date, expiration_date, status
  * 12.1 Первый элемент массива заполняется следующими значениями: departure_date, transfer_date, lit("transfer")
  * 12.2 Второй элемент массива заполняется следующими значениями: transfer_date, delivery_date, lit("delivered")
  * 13. Выполнить внутренне соединение DataFrame из п.10 и п.12
  * 14. Полученный результат преобразовать в плоскую таблицу используя функцию explode для поля с массивом
  * 15. Вывести результат используя метод show() или записать DataFrame в файл
  * */

}
