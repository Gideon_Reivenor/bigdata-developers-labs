package ru.philit.bigdata.vsu.spark.exercise

object Lab3Join {
  /*
   * Lab3 - пример использования join
   * Расчитать кто и сколько сделал заказов, за какие даты, на какую сумму.
   * Итоговое множество содержит поля: customer.name, order.order.orderDate, sum(order.numberOfProduct * product.price)
   *
   * 1. Создать экземпляр класса SparkConf
   * 2. Установить мастера на local[*] и установить имя приложения
   * 3. Создать экземпляр класса SparkContext, используя объект SparkConf
   * 4. Загрузить в RDD файл src/test/resources/input/order
   * 5. Используя класс [[ru.phil_it.bigdata.entity.Order]], распарсить строки RDD
   * 6. Выбрать ключ поле (customerID), в значение (orderDate, numberOfProduct, productID)
   * 7. Загрузить в RDD файл src/test/resources/input/customer
   * 8. Используя класс [[ru.phil_it.bigdata.entity.Customer]], распарсить строки RDD
   * 9. Выбрать ключ поле (id), в значение (name)
   * 10. Выполнить внутреннее соединение RDD из п.6 и п.9
   * 11. Выбрать ключ (productID), в значение (customer.name, orderDate,  numberOfProduct)
   * 12. Загрузить в RDD файл src/test/resources/input/product
   * 13. Используя класс [[ru.phil_it.bigdata.entity.Product]], распарсить строки RDD
   * 14. Выбрать ключ (id), значение (price)
   * 15. Выполнить внутреннее соединение с RDD из п.11 и п.14
   * 16. Выбрать ключ (customer.name, order.orderDate), значение (order.numberOfProduct * product.price)
   * 17. Расчитать сумму в значении по ключу
   * 18. Вывести результат или записать в директорию src/test/resources/output/lab3
   * */
}
