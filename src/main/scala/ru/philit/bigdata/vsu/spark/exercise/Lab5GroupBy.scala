package ru.philit.bigdata.vsu.spark.exercise

object Lab5GroupBy {
  /*
 * Lab5 - пример использования groupByKey
 * Определить средний объем заказа, за всё время, для каждого заказчика
 * Итоговое множество содержит поля: order.customerID, sum(order.numberOfProduct),
 * count(order.numberOfProduct), sum(order.numberOfProduct) / count(order.numberOfProduct)
 *
 * 1. Создать экземпляр класса SparkConf
 * 2. Установить мастера на local[*] и установить имя приложения
 * 3. Создать экземпляр класса SparkContext, используя объект SparkConf
 * 4. Загрузить в RDD файд src/test/resources/input/order
 * 5. Используя класс [[ru.phil_it.bigdata.entity.Order]], распарсить строки в RDD
 * 6. Выбрать только те транзакции у которых статус delivered
 * 7. Выбрать ключ (customerID), значение (numberOfProducts)
 * 8. Выполнить группировку по ключу
 * 9. Посчитать сумму по значению и разделить на размер коллекции
 * 10. Вывести результат или записать в директорию src/test/resources/output/lab5
 * */
}
