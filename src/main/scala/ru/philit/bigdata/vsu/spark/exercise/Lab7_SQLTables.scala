package ru.philit.bigdata.vsu.spark.exercise

object Lab7_SQLTables {
  /*
   * Lab7 - пример считывания таблиц
   * Создать объект SparkSession
   * Загрузить таблицу используя метод table/sql
   *
   * 1. Создать неявный объект SparkSession, используя метод builder()
   * 2. Установить master в local[*]
   * 3. Установить имя приложения spark-sql-labs
   * 4. Создать неявный объект класса Parameters, используя метод Parameters.instance
   * 5. Вызвать метод initTables класса Parameters
   * 6. Загрузить таблицу customer, используя метод table()
   * 7. Загрузить таблицу product, используя метод sql() и в качестве аргумента запрос: "FROM product"
   * 8. Загрузить таблицу order, используя метод sql и в качестве аргумента запрос:
   * "SELECT DISTINCT customer_id, order_date FROM order WHERE status = 'delivered'"
   * 9. Вывести таблицы используя метод show()
   * в качестве аргумента можно указать лимит в кол-во строк и тип выводимой строки полный/урезанный
   * */

}
