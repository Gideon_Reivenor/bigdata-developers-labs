package ru.philit.bigdata.vsu.spark.exercise

object Lab8TableColumns {
  /*
   * Lab8 - пример обработки полей при помощи select/withColumns
   * Вывести название всех устройств,
   * если цена больее 50000 вычесть 10% от стоимости назвать поле new_price,
   * добавить поле type, используя функцию getTypeDevice
   * Итоговое множество содержит поля: product.name, new_price, type
   *
   * 1. Создать неявный объект SparkSession, используя метод builder()
   * 2. Установить master в local[*]
   * 3. Установить имя приложения spark-sql-labs
   * 4. Создать неявный объект класса Parameters, используя метод Parameters.instance
   * 5. Вызвать метод initTables класса Parameters
   * 6. Загрузить таблицу product в DataFrame
   * 7. В import udf добавить так же when и col из того же пакета
   * 8. В методе select() выбрать поле name, используя функцию when().otherwise()
   * расчитать новую стоимость девайса
   * 9. Используя метод withColumn и функцию getTypeDevice, добавить к выборке поле type
   * 10. Вывести результат используя метод show() или записать DataFrame в файл
   *
   * P.S.: для более удобного обращения к полям можно использовать символ ' вместо метода col()
   * Для этого необходимо выполнить import spark.implicits._
   * где spark экземпляр класса SparkSession
   * Пример: spark.table("t1").select(col("name"), 'email)
   *
   * */
}
