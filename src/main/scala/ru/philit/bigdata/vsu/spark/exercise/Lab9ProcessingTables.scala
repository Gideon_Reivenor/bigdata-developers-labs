package ru.philit.bigdata.vsu.spark.exercise

object Lab9ProcessingTables {
  /*
   * Lab9 - пример использования filter, join, crossJoin, broadcast, orderBy
   * Вывести информацию о клиенте email, название продукта
   * и кол-во доставленного товара за первую половину 2018 года
   * Итоговое множество содержит поля: customer.email, product.name, order.order_date, order.number_of_product
   *
   * 1. Создать неявный объект SparkSession, используя метод builder()
   * 2. Установить master в local[*]
   * 3. Установить имя приложения spark-sql-labs
   * 4. Создать неявный объект класса Parameters, используя метод Parameters.instance
   * 5. Вызвать метод initTables класса Parameters
   * 6. Загрузить в DataFrame таблицу order
   * 7. Загрузить в DataFrame таблицу customer, выбрать поля:
   *    id далее customer_id, email
   * 8. Загрузить в DataFrame таблицу product, выбрать поля:
   *    id далее product_id, name далее product_name
   * 9. Выполнить перекрестное соединение DataFrame из п.7 и п.8
   * 10. Выбрать транзакции co статусом "delivered" и датой заказа с 2018-01-01 по 2018-06-30
   * 11. Выполнить внутреннее соединение по полю customer_id, product_id использую broadcast
   * 12. Выбрать поля email, product_name, order_date, number_of_products
   * 13. Выполнить сортировку по полю email, order_date
   * 14. Вывести результат используя метод show() или записать DataFrame в файл
   * */
}
