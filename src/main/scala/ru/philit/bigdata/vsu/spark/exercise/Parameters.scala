package ru.philit.bigdata.vsu.spark.exercise

import java.io.File

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.types.StructType
import ru.philit.bigdata.vsu.spark.exercise.domain._

object Parameters {
  val POPULATION_DATASET_PATH = ""
  val EXAMPLE_OUTPUT_PATH = "./output/"


  val path_customer = "./datasource/customer/*"
  val path_order = "./datasource/order/*"
  val path_product = "./datasource/product/*"
  val path_order_info: String = "./datasource" + File.separator + "order-info/*"

  val table_customer = "customer"
  val table_order = "order"
  val table_product = "product"
  val table_order_info = "order_info"

  private def createTable(name: String, structType: StructType, path: String, delimiter: String = "\\t")
                         (implicit spark: SparkSession): Unit = {
    spark.read
      .format("com.databricks.spark.csv")
        //.option("inferSchema", "true")
      .options(
        Map(
          "delimiter" -> delimiter,
          "nullValue" -> "\\N"
        )
      ).schema(structType).load(path).createOrReplaceTempView(name)
  }

  def initTables(implicit spark: SparkSession): Unit = {
    createTable(Parameters.table_order, Order.structType, Parameters.path_order)
    createTable(Parameters.table_product, Product.structType, Parameters.path_product)
    createTable(Parameters.table_customer, Customer.structType, Parameters.path_customer)
    createTable(Parameters.table_order_info, OrderInfo.structType, Parameters.path_order_info)
  }
}
