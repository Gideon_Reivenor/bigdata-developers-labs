package ru.philit.bigdata.vsu.spark.exercise.domain

import java.sql.Date

import org.apache.spark.sql.types._

object Customer extends Enumeration {

  private val DELIMITER = "\\t"

  val ID, NAME, EMAIL, JOIN_DATE, STATUS = Value

  val structType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(NAME.toString, StringType),
      StructField(EMAIL.toString, StringType),
      StructField(JOIN_DATE.toString, DateType),
      StructField(STATUS.toString, StringType)
    )
  )

  def apply(row: String): Customer = {
    val array = row.split(DELIMITER, -1)
    Customer(
      array(ID.id).toInt,
      array(NAME.id),
      array(EMAIL.id),
      Date.valueOf(array(JOIN_DATE.id)),
      array(STATUS.id)
    )
  }
}

case class Customer(id: Int,
                    name: String,
                    email: String,
                    joinDate: Date,
                    status: String)
