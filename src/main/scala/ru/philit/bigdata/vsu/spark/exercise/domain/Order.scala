package ru.philit.bigdata.vsu.spark.exercise.domain

import java.sql.Date

import org.apache.spark.sql.types._

object Order extends Enumeration {

  private val DELIMITER = "\\t"

  val CUSTOMER_ID, ORDER_ID, PRODUCT_ID, NUMBER_OF_PRODUCT, ORDER_DATE, STATUS = Value

  val structType = StructType(
    Seq(
      StructField(CUSTOMER_ID.toString, IntegerType),
      StructField(ORDER_ID.toString, IntegerType),
      StructField(PRODUCT_ID.toString, IntegerType),
      StructField(NUMBER_OF_PRODUCT.toString, IntegerType),
      StructField(ORDER_DATE.toString, DateType),
      StructField(STATUS.toString, StringType)
    )
  )

  def apply(row: String): Order = {
    val array = row.split(DELIMITER, -1)
    Order(
      array(CUSTOMER_ID.id).toInt,
      array(ORDER_ID.id).toInt,
      array(PRODUCT_ID.id).toInt,
      array(NUMBER_OF_PRODUCT.id).toInt,
      Date.valueOf(array(ORDER_DATE.id)),
      array(STATUS.id)
    )
  }
}

case class Order(customerID: Int, orderID: Int, productID: Int, numberOfProduct: Int, orderDate: Date, status: String)