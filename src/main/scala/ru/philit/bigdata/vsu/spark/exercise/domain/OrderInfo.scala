package ru.philit.bigdata.vsu.spark.exercise.domain

import java.sql.Date

import org.apache.spark.sql.types._

object OrderInfo extends Enumeration {

  private val DELIMITER: String = "\\t"

  val ID, DEPARTURE_DATE, TRANSFER_DATE, DELIVERY_DATE, DEPARTURE_CITY, DELIVERY_CITY = Value

  val structType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(DEPARTURE_DATE.toString, DateType),
      StructField(TRANSFER_DATE.toString, DateType),
      StructField(DELIVERY_DATE.toString, DateType),
      StructField(DEPARTURE_CITY.toString, StringType),
      StructField(DELIVERY_CITY.toString, StringType)
    )
  )

  def apply(row: String): OrderInfo = {
    val array = row.split(DELIMITER, -1)
    OrderInfo(
      array(ID.id).toInt,
      Date.valueOf(array(DEPARTURE_DATE.id)),
      Date.valueOf(array(TRANSFER_DATE.id)),
      Date.valueOf(array(DELIVERY_DATE.id)),
      array(DEPARTURE_CITY.id),
      array(DELIVERY_CITY.id)
    )
  }
}

case class OrderInfo(id: Int, departureDate: Date, transferDate: Date, deliveryDate: Date, departureCity: String, deliveryCity: String)
