package ru.philit.bigdata.vsu.spark.exercise.domain

import org.apache.spark.sql.types._

object Product extends Enumeration {

  private val DELIMITER = "\\t"

  val ID, NAME, PRICE, NUMBER_OF_PRODUCTS = Value

  val structType = StructType(
    Seq(
      StructField(ID.toString, IntegerType),
      StructField(NAME.toString, StringType),
      StructField(PRICE.toString, DoubleType),
      StructField(NUMBER_OF_PRODUCTS.toString, IntegerType)
    )
  )

  def apply(row: String): Product = {
    val array = row.split(DELIMITER, -1)
    Product(
      array(ID.id).toInt,
      array(NAME.id),
      array(PRICE.id).toDouble,
      array(NUMBER_OF_PRODUCTS.id).toInt
    )
  }
}

case class Product(id: Int, name: String, price: Double, numberOfProducts: Int)