import org.apache.log4j.Logger
import org.apache.spark.{SparkConf, SparkContext}

Logger.getLogger("org.apache.spark")
Logger.getLogger("netty")


val sparkConf = new SparkConf()
  .setAppName("spark-example")
  .setMaster("local[*]")
val sc = new SparkContext(sparkConf)

case class AggregateResult(sum: Int, count: Int)

def aggregate(acc: AggregateResult, value: Int): AggregateResult =
  acc.copy(
    sum = acc.sum + value,
    count = acc.count + 1)

def merge(acc1: AggregateResult, acc2: AggregateResult): AggregateResult =
  acc1.copy(
    sum = acc1.sum + acc2.sum,
    count = acc1.count + acc2.count)



val tenRdd = sc.parallelize(1 to 10)

tenRdd.aggregate(AggregateResult(0, 0))(aggregate, merge)
//    .aggregate(AggregateResult(0, 0))(aggregate, merge)
val anotherRDD = sc.parallelize(List(1, 2, 3, 4, 5))


tenRdd.union(anotherRDD).foreach(println(_))









sc.stop()