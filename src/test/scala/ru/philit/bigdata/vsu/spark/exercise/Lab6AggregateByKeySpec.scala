package ru.philit.bigdata.vsu.spark.exercise

import java.sql.Date
import java.time.LocalDate

import com.holdenkarau.spark.testing.{RDDComparisons, SharedSparkContext}
import org.apache.spark.rdd.RDD
import org.junit.Before
import org.scalatest.BeforeAndAfterAll
import org.scalatest.flatspec.AnyFlatSpec
import ru.philit.bigdata.vsu.spark.exercise.domain.Order

class Lab6AggregateByKeySpec
  extends AnyFlatSpec
    with SharedSparkContext
    with RDDComparisons {

  "Aggregate function" should "Return correct aggregation" in {
    val input = sc.parallelize(Seq(
      Order(1, 23, 1, 4, Date.valueOf(LocalDate.now()), "delivered"),
      Order(2, 21, 1, 5, Date.valueOf(LocalDate.now()), "delivered"),
      Order(1, 20, 2, 1, Date.valueOf(LocalDate.now()), "canceled")
    ))
    val expected: RDD[BuyingStats] = sc.parallelize(
      Seq(
        BuyingStats(1, 1, 4, 4, 4),
        BuyingStats(2, 1, 5, 5, 5)
      )
    )

    val result = Lab6AggregateBy.getProductsStatistics(input)

    assert(compareRDD(expected, result).isEmpty)
    //assertRDDEquals[BuyingStats](expected, result)
  }

  "When filtration changed" should "Result also changed" in {

    val input = sc.parallelize(Seq(
      Order(1, 23, 1, 4, Date.valueOf(LocalDate.now()), "delivered"),
      Order(2, 21, 1, 5, Date.valueOf(LocalDate.now()), "delivered"),
      Order(1, 20, 2, 1, Date.valueOf(LocalDate.now()), "canceled")
    ))
    val result = Lab6AggregateBy.getProductsStatistics(input, order => order.status != "delivered")

    assert(compareRDD(
      sc.parallelize(Seq(BuyingStats(1, 1, 1, 1, 1))),
      result
    ).isEmpty)
  }
}
